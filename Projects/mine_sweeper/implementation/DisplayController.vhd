LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;

ENTITY DisplayController IS
	generic(
		MapDimension		:		INTEGER := 10;
		TilePickBusSize		:		INTEGER := 4;
		BombCountBusSize	:		INTEGER := 4
	);
	PORT (
		--Requisicoes ao MapController:
		TileReadX			: out	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);
		TileReadY			: out	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);
		--Retorno do MapController:
		TileBomb			: in	STD_LOGIC;
		TileStateBits		: in	STD_LOGIC_VECTOR(1 downto 0);
		CloseBombsCount		: in	STD_LOGIC_VECTOR(BombCountBusSize-1 downto 0);

		reset				: IN STD_LOGIC;

		clk27M				: IN STD_LOGIC;

		VGA_R, VGA_G, VGA_B	: OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		VGA_HS, VGA_VS		: OUT STD_LOGIC
	);
END ENTITY;
--Declaracao do componente VGACON - nao precisa mexer:
ARCHITECTURE behavior OF DisplayController IS
	
	component memory is

	GENERIC 
	(
		WORDSIZE		: NATURAL	:= 8;
		BITS_OF_ADDR	: NATURAL	:= 16;
		MIF_FILE		: STRING	:= ""
	);

	PORT 
	(
		clock   : IN	STD_LOGIC;
		we      : IN	STD_LOGIC;
		address : IN	STD_LOGIC_VECTOR(BITS_OF_ADDR-1 DOWNTO 0);
		datain  : IN	STD_LOGIC_VECTOR(WORDSIZE-1 DOWNTO 0);
		dataout : OUT	STD_LOGIC_VECTOR(WORDSIZE-1 DOWNTO 0)
	);
	
	end component;
	
	COMPONENT vgacon IS
		GENERIC (
			NUM_HORZ_PIXELS : NATURAL := 10;	-- Number of horizontal pixels
			NUM_VERT_PIXELS : NATURAL := 10		-- Number of vertical pixels
		);
		PORT (
			clk27M, rstn              : IN STD_LOGIC;
			write_clk, write_enable   : IN STD_LOGIC;
			write_addr                : IN INTEGER RANGE 0 TO NUM_HORZ_PIXELS * NUM_VERT_PIXELS - 1;
			data_in                   : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
			red, green, blue          : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
			hsync, vsync              : OUT STD_LOGIC
		);
	END COMPONENT;


	
	CONSTANT CONS_CLOCK_DIV : INTEGER := 10;
	CONSTANT HORZ_SIZE : INTEGER := 128;
	CONSTANT VERT_SIZE : INTEGER := 96;
	
	SIGNAL slow_clock : STD_LOGIC;
	
	SIGNAL	clear_video_address	,
			normal_video_address,
			video_address			: INTEGER RANGE 0 TO HORZ_SIZE * VERT_SIZE - 1;
	
	SIGNAL	clear_video_word,
			normal_video_word,
			video_word				: STD_LOGIC_VECTOR (2 DOWNTO 0);
	
	TYPE VGA_STATES IS (NORMAL, CLEAR);
	SIGNAL state : VGA_STATES;
	
	signal SquareROMAddress	:	STD_LOGIC_VECTOR(9 downto 0);
	signal SquareROMOutput	:	STD_LOGIC_VECTOR(2 downto 0);
BEGIN

	--Instanciação do componente VGACON:
	vga_component: vgacon
	GENERIC MAP (
		NUM_HORZ_PIXELS => HORZ_SIZE,
		NUM_VERT_PIXELS => VERT_SIZE
	) PORT MAP (
		clk27M			=> clk27M		,
		rstn			=> reset		,
		write_clk		=> clk27M		,
		write_enable	=> '1'			,
		write_addr      => video_address,
		data_in         => video_word	,
		red				=> VGA_R		,
		green			=> VGA_G		,
		blue			=> VGA_B		,
		hsync			=> VGA_HS		,
		vsync			=> VGA_VS		
	);
	
	video_word <= normal_video_word WHEN state = NORMAL ELSE clear_video_word;
	
	video_address <= normal_video_address WHEN state = NORMAL ELSE clear_video_address;

	clock_divider:
	PROCESS (clk27M, reset)
		VARIABLE i : INTEGER := 0;
	BEGIN
		IF (reset = '0') THEN
			i := 0;
			slow_clock <= '0';
		ELSIF (rising_edge(clk27M)) THEN
			IF (i <= CONS_CLOCK_DIV/2) THEN
				i := i + 1;
				slow_clock <= '0';
			ELSIF (i < CONS_CLOCK_DIV-1) THEN
				i := i + 1;
				slow_clock <= '1';
			ELSE		
				i := 0;
			END IF;	
		END IF;
	END PROCESS;
	
	vga_clear:
	PROCESS(clk27M, reset, clear_video_address)
	BEGIN
		IF (reset = '0') THEN
			state <= CLEAR;
			clear_video_address <= 0;
			clear_video_word <= "000";
		ELSIF (rising_edge(clk27M)) THEN
			CASE state IS
				WHEN CLEAR =>
					clear_video_address <= clear_video_address + 1;
					clear_video_word <= "000";
					IF (clear_video_address < HORZ_SIZE * VERT_SIZE-1) THEN
						state <= CLEAR;
					ELSE
						state <= NORMAL;
					END IF;
				WHEN NORMAL =>
					state <= NORMAL;
			END CASE;
		END IF;	
	END PROCESS;
		



	SquareImagesROM: memory
	GENERIC map
	(
		WORDSIZE		=> 3,
		BITS_OF_ADDR	=> 10,
		MIF_FILE		=> "squares.mif"
	)

	PORT map
	(
		clock   => '0',
		we      => '0',
		address => SquareROMAddress,
		datain  => "000",
		dataout => SquareROMOutput
	);






	vga_writer:
	PROCESS (slow_clock, reset, normal_video_address)

	
	VARIABLE UpdateOnFallingEdge		:	BOOLEAN;
	
	VARIABLE PosX,PosY					:	INTEGER;
	VARIABLE TileX,TileY				:	INTEGER;
	VARIABLE TilePixelX,TilePixelY		:	INTEGER;
	
	VARIABLE TileImage					:	INTEGER;
	

	BEGIN
		IF (reset = '0') THEN
			normal_video_address <= 0;
			normal_video_word <= "000";
		ELSIF (rising_edge(slow_clock)) THEN
			normal_video_address <= normal_video_address + 1;
			if (normal_video_address >= VERT_SIZE * HORZ_SIZE) THEN
				normal_video_address <= 0;
			end if;

			--Processamento para descobrir qual pixel imprimir vai aqui:
			
			--Pegar as coordenada X,Y que estamos desenhando:
			PosX := (normal_video_address  mod HORZ_SIZE);
			PosY := (normal_video_address / HORZ_SIZE);
			
			
			--Identificar se estamos dentro do campo minado:
			if (PosX >= 25 AND PosX <= 104 AND PosY >= 16) then
				
				TileX := (PosX - 24)/8;
				TileY := (PosY - 16)/8;
				
				TileReadX <= std_logic_vector(to_unsigned(TileX,TilePickBusSize));
				TileReadY <= std_logic_vector(to_unsigned(TileY,TilePickBusSize));
				
				case TileStateBits is
					when "00" =>
						TileImage := 0;
					when "01" =>
						TileImage := 11;
					when "10" =>
						TileImage := 10;
					when "11" =>
						if (TileBomb = '1') then
							TileImage := 12;
						else
							--vai depender
							TileImage := 1 + to_integer(unsigned(CloseBombsCount));
						end if;
				end case;
						
						
				
				TilePixelX := (PosX - 24 - 1) mod 8;
				TilePixelY := (PosY - 16) mod 8;
				
				SquareROMAddress <= std_logic_vector(to_unsigned(TileImage * 64 + TilePixelY * 8 + TilePixelX,10));
				normal_video_word <= SquareROMOutput;

				
			else
				normal_video_word <= "000";
				
			end if;

		END IF;	
	END PROCESS;
END ARCHITECTURE;