LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;

entity Tile is

	port
	(
		clk				: in	STD_LOGIC;

		Command			: in	STD_LOGIC_VECTOR(1 downto 0);
		CloseBombs		: in	STD_LOGIC_VECTOR(7 downto 0);

		BombOut			: out	STD_LOGIC;
		BlewUp			: out	STD_LOGIC;
		Danger			: out	STD_LOGIC;
		CloseBombsCount	: out	STD_LOGIC_VECTOR(2 downto 0)

	);
end Tile;-- Library Clause(s) (optional)
-- Use Clause(s) (optional)

architecture struct of Tile is

	type tileState IS (Free, Bomb, Revealed, MarkedFree, DoubtFree, Blown, MarkedBomb, DoubtBomb);
	signal state : tileState := Free;

begin
	--Processar a contagem de bombas:
	process(CloseBombs)

	variable count : natural := 0;

	begin
		for i in 0 to 7 loop
			if CloseBombs(i) = '1' then
				count := count + 1; 
			end if;
		end loop;
		CloseBombsCount <= std_logic_vector(to_unsigned(count, 3));
	end process;


	--Processamento de comandos recebidos:
	process(clk)
	begin
		if (clk'EVENT AND clk = '1') then
			case Command IS
				--Reset Command:
				when "11" =>
					state <= Free;
				--Activate Command:
				when "10" =>
					if (state = Free) then
						state <= Revealed;
					elsif (state = Bomb) then
						state <= Blown;
					end if;
				--Mark Command:
				when "01" =>
					case state is
						when Free =>
							state <= MarkedFree;
						when MarkedFree =>
							state <= DoubtFree;
						when DoubtFree =>
							state <= Free;
						when Bomb =>
							state <= MarkedBomb;
						when MarkedBomb =>
							state <= DoubtBomb;
						when DoubtBomb =>
							state <= Bomb;
						when others =>
							state <= state;
					end case;
				--Bomb Set Command:
				when "00" =>
					state <= Bomb;
			end case;
		end if;
	end process;

	--Administração das flags de saida:
	--Resumo:
	---Bomb		: indica que o tile tem/tinha uma bomba
	---BlewUp	: indica se o tile explodiu
	---Danger	: indica se o tile não esta como deveria estar para vencer o jogo
	process(state)
	begin
		case state is
			when Free =>
				BombOut <= '0';
				BlewUp <= '0';
				Danger <= '1';
			when Bomb =>
				BombOut <= '1';
				BlewUp <= '0';
				Danger <= '1';
			when Revealed =>
				BombOut <= '0';
				BlewUp <= '0';
				Danger <= '0';
			when MarkedFree =>
				BombOut <= '0';
				BlewUp <= '0';
				Danger <= '1';
			when DoubtFree =>
				BombOut <= '0';
				BlewUp <= '0';
				Danger <= '1';
			when Blown =>
				BombOut <= '1';
				BlewUp <= '1';
				Danger <= '1';
			when MarkedBomb =>
				BombOut <= '1';
				BlewUp <= '0';
				Danger <= '0';
			when DoubtBomb =>
				BombOut <= '1';
				BlewUp <= '0';
				Danger <= '1';
		end case;
	end process;

	

end struct;

