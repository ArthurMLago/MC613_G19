LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY Packages;
USE Packages.decoders_package.all;
USE Packages.registers_package.all;
USE Packages.tristate_package.all;

entity bank is

	GENERIC 
	(
		WORDSIZE : NATURAL := 32
	);
	PORT 
	(
		WR_EN, RD_EN, -- enable de escrita, enable de leitura
		clear, -- clear
		clock		: IN 	STD_LOGIC; -- clock
		WR_ADDR, -- endereço de escrita
		RD_ADDR1, -- dois endereços de leitura
		RD_ADDR2	: IN	STD_LOGIC_VECTOR (4 DOWNTO 0);
		DATA_IN		: IN	STD_LOGIC_VECTOR (WORDSIZE-1 DOWNTO 0);
		DATA_OUT1, -- duas leituras simultâneas
		DATA_OUT2	: OUT	STD_LOGIC_VECTOR (WORDSIZE-1 DOWNTO 0)
	);

end bank;

architecture Structure of bank is

	signal D2R	:STD_LOGIC_VECTOR(31 downto 0); -- decoder to register 
	
	signal R2D_1	:STD_LOGIC_VECTOR(31 downto 0); -- register to decoder 1
	signal R2D_2	:STD_LOGIC_VECTOR(31 downto 0); -- register to decoder 2

	-- mascara de escrita - para nunca se alterar o r0 --
	signal WriteMask	: STD_LOGIC_VECTOR(31 downto 0);

begin

	WriteMask <= (0 => '0' , OTHERS => '1');
	
	GenerateRegisters: 
		for i in 31 downto 0 generate -- 32 registradores de 32 bits

			signal R2T	:STD_LOGIC_VECTOR(WORDSIZE - 1 downto 0); -- register to tristate

			begin

				-- registradores genericos -- 
				Registers: nRegLsRa
					generic map
					(
						N => WORDSIZE -- tamanho da palavra (vai ser 32 em sua instancia)
					)
					port map 
					(
						D		=> DATA_IN, 
						Clock	=> clock,
						Reset	=> clear OR NOT WriteMask(i),
						-- AND port utilizada 	para não haver escrita no r0 --
						Load	=> D2R(i) AND WriteMask(i),
						Q		=> R2T
					);

				Tristates1: tristate
					generic map
					(
						N => WORDSIZE
					)
					port map
					(
						e => R2D_1(i),
						x => R2T,
						f => DATA_OUT1
					);
				Tristates2: tristate
					generic map
					(
						N => WORDSIZE
					)
					port map
					(
						e => R2D_2(i),
						x => R2T,
						f => DATA_OUT2
					);
		end generate;

	-- decoder da entrada --
	InputRegisterSelector : generic_decoder
		generic map
		(
			InputBusSize => 5
		)
		port map 
		(
			 Enable	=> WR_EN,
			 S		=> WR_ADDR,
			 D		=> D2R 
		);

	OutputRegisterSelector1 : generic_decoder
		generic map
		(
			InputBusSize => 5
		)
		port map 
		(
			Enable	=> RD_EN,
			S		=> RD_ADDR1,
			D		=> R2D_1
		);

	OutputRegisterSelector2 : generic_decoder
		generic map
		(
			InputBusSize => 5
		)
		port map 
		(
			Enable	=> RD_EN,
			S		=> RD_ADDR2,
			D		=> R2D_2
		);

end Structure;
