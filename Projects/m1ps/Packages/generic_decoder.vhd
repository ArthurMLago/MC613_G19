LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;


ENTITY generic_decoder IS
	GENERIC (
		InputBusSize	: INTEGER  := 5
	);
	PORT (
		S				: IN STD_LOGIC_VECTOR(InputBusSize -1 DOWNTO 0);
		Enable			: IN STD_LOGIC;

		D				: buffer STD_LOGIC_VECTOR(2**InputBusSize - 1 DOWNTO 0)
		);
END generic_decoder;

ARCHITECTURE Structure OF generic_decoder IS
	
 begin
 
	process(S,Enable)
	begin
		D <= (others => '0');
		if (Enable = '1') then
			D(to_integer(unsigned(S))) <= '1';
		end if;
	end process;

END Structure ;