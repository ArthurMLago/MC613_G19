-- tri state  --

library ieee;
use ieee.std_logic_1164.all;

entity tristate is
	generic (N	: integer  :=	4);
	port
	(
		-- Input ports
		e	: in STD_LOGIC;
		x	: in STD_LOGIC_VECTOR(N - 1 downto 0);
		
		-- Output ports
		f	: out STD_LOGIC_VECTOR(N - 1 downto 0)
	);
end tristate;

-- architecture
architecture Behavior of tristate is	
begin 

	f <= x when e = '1' else (others => 'Z');
		
end Behavior;
 