LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY G19_packages;
USE G19_packages.flipflops_package.all;

entity linFeedbackShiftReg is
	generic
	(
		N		: INTEGER  :=	32
	);
	port
	(
		-- Input ports
		Clock		: in STD_LOGIC;

		LoadSeed	: in STD_LOGIC;
		SeedSet		: in STD_LOGIC_VECTOR(N - 1 downto 0);

		LoadTaps	: in STD_LOGIC;
		TapsSet		: in STD_LOGIC_VECTOR(N - 1 downto 0);

		-- Output ports
		Q			: buffer STD_LOGIC_VECTOR(N - 1 downto 0)
	);
end linFeedbackShiftReg;

architecture Structure of linFeedbackShiftReg is


	component latchD is
	
	port
	(
		-- Input ports
		D, clk	: in  STD_LOGIC;
		
		-- Output ports
		Q		: out STD_LOGIC
	);
end component;



	signal RClock		: STD_LOGIC;
	signal Taps			: STD_LOGIC_VECTOR(N - 1 downto 0);
	
	signal AndSignal	: STD_LOGIC_VECTOR(N - 1 downto 0);
	signal XorSignal	: STD_LOGIC_VECTOR(N downto 0);

begin
	
	RClock <= Clock AND not LoadSeed;
	
	XorSignal(N) <= '0';

	--Generate flipFlops:
	GenerateStages: 
		for i in 0 to N - 1 generate
			begin

			AndSignal(i) <= Q(i) AND Taps(i);
			XorSignal(i) <= XorSignal(i + 1) XOR AndSignal(i);

			LOWER_BIT: if i = 0 generate
				FlipFlops: flipflopD
					port map(
						D		=> XorSignal(0),
						preset	=> SeedSet(i) AND LoadSeed,
						reset	=> NOT SeedSet(i) AND LoadSeed,
						clk		=> RClock,
						Q		=> Q(i),
						Qn		=> open
					);
			end generate LOWER_BIT;

			UPPER_BITS: if i > 0 generate
				FlipFlops: flipflopD
					port map(
						D		=> Q(i - 1),
						preset	=> SeedSet(i) AND LoadSeed,
						reset	=> NOT SeedSet(i) AND LoadSeed,
						clk		=> RClock,
						Q		=> Q(i),
						Qn		=> open
					);
				end generate UPPER_BITS;
		end generate;




	--Taps set:
	GenerateSeeders: 
	for i in 0 to N - 2 generate
		begin
		Latches: latchD
			port map(
				D	=> TapsSet(i),
				clk	=> LoadTaps,
				Q	=> Taps(i)
			);
	end generate;



end Structure;
