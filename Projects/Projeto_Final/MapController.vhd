LIBRARY ieee;


USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MapController is

	generic(
		MapDimension		:		INTEGER := 10;
		TilePickBusSize		:		INTEGER := 4;
		CommandBusSize		:		INTEGER := 2;
		BombCountBusSize	:		INTEGER := 4
	);

	port
	(
		--Sinais de entrada para escrita nos Tiles:
		WriteCLK			: in	STD_LOGIC;
		Command				: in	STD_LOGIC_VECTOR(CommandBusSize - 1 downto 0);
		WriteToAll			: in	STD_LOGIC;
		TileWriteX			: in	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);
		TileWriteY			: in	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);

		--Sinais de entrada para leitura dos Tiles:
		ReadCLK				: in	STD_LOGIC;
		TileReadX			: in	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);
		TileReadY			: in	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);
		--Sinais de saida para leitura dos Tiles:
		TileBomb			: out	STD_LOGIC;
		TileStateBits		: out	STD_LOGIC_VECTOR(1 downto 0);
		CloseBombsCount		: out	STD_LOGIC_VECTOR(BombCountBusSize - 1 downto 0);


		--Sinais de saida para o GameController e MapCreator
		BombCount			: out	STD_LOGIC_VECTOR(BombCountBusSize - 1 downto 0);
		GameOver			: out	STD_LOGIC;
		Victory				: out	STD_LOGIC



	);
end MapController;-- Library Clause(s) (optional)
-- Use Clause(s) (optional)

architecture struct of MapController is


COMPONENT Tile is

	port
	(
		clk				: in	STD_LOGIC;

		Command			: in	STD_LOGIC_VECTOR(1 downto 0);
		CloseBombs		: in	STD_LOGIC_VECTOR(7 downto 0);

		BombOut			: out	STD_LOGIC;
		StateBits		: out	STD_LOGIC_VECTOR(1 downto 0);
		CloseBombsCount	: out	STD_LOGIC_VECTOR(3 downto 0)

	);
end COMPONENT;


	type ArrayVEC is array (0 to MapDimension**2 - 1) of std_logic_vector(3 downto 0);

	signal CloseBombsSignal	: ArrayVEC;


	signal BombSignal : STD_LOGIC_VECTOR((MapDimension + 2)**2 downto 0);
	signal StateBit0Signal : STD_LOGIC_VECTOR(MapDimension**2 - 1 downto 0);
	signal StateBit1Signal : STD_LOGIC_VECTOR(MapDimension**2 - 1 downto 0);


begin
	GenerateTilesColum: 
		for i in MapDimension - 1 downto 0 generate
			GenerateTilesRow:

				for j in MapDimension - 1 downto 0 generate
				
					signal TempWriteCLK : STD_LOGIC;
					begin
					
					process(WriteCLK)
					begin
						--Cuidado!!! se o valor de WriteCLK for alto normalmente, a cada mudanca de Tile, havera uma borda de subida!
						if (((to_integer(unsigned(TileWriteX)) = i) AND (to_integer(unsigned(TileWriteY)) = j)) OR WriteToAll = '1') then 
							TempWriteCLK <= WriteCLK;
						else
							TempWriteCLK <= '0';
						end if;
						
					end process;


					TileInstance : Tile
						port map 
						(
							clk 			=> TempWriteCLK,
							Command 		=> Command,
							CloseBombs(0)	=> BombSignal(((i    ) * (MapDimension + 2)) + j    ),
							CloseBombs(1)	=> BombSignal(((i    ) * (MapDimension + 2)) + j + 1),
							CloseBombs(2)	=> BombSignal(((i    ) * (MapDimension + 2)) + j + 2),
							CloseBombs(3)	=> BombSignal(((i + 1) * (MapDimension + 2)) + j + 2),
							CloseBombs(4)	=> BombSignal(((i + 2) * (MapDimension + 2)) + j + 2),
							CloseBombs(5)	=> BombSignal(((i + 2) * (MapDimension + 2)) + j + 1),
							CloseBombs(6)	=> BombSignal(((i + 2) * (MapDimension + 2)) + j    ),
							CloseBombs(7)	=> BombSignal(((i + 1) * (MapDimension + 2)) + j    ),

							BombOut			=> BombSignal(((i + 1) * (MapDimension + 2)) + j + 1),
							StateBits(0)	=> StateBit0Signal((i * MapDimension) + j),
							StateBits(1)	=> StateBit1Signal((i * MapDimension) + j),
							CloseBombsCount	=> CloseBombsSignal((i * MapDimension) + j)
						);


				end generate;
		end generate;

	process(ReadCLK)

	begin
		if (ReadCLK'EVENT AND ReadCLK = '1') then
			TileBomb <= BombSignal(((to_integer(unsigned(TileReadX)) + 1)*(MapDimension + 2)) + to_integer(unsigned(TileReadY)) + 1);
			TileStateBits(0) <= StateBit0Signal(((to_integer(unsigned(TileReadX)))*MapDimension) + to_integer(unsigned(TileReadY)));
			TileStateBits(1) <= StateBit1Signal(((to_integer(unsigned(TileReadX)))*MapDimension) + to_integer(unsigned(TileReadY)));
			CloseBombsCount <= CloseBombsSignal(((to_integer(unsigned(TileReadX)))*MapDimension) + to_integer(unsigned(TileReadY)));
		end if;
		
	end process;





end struct;
