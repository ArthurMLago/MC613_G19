library ieee;
use ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;

entity demo_setup_vhd is
	port
	(
		-- Input ports
		SW			: in  STD_LOGIC_VECTOR(9 DOWNTO 0);
		KEY			: in  STD_LOGIC_VECTOR(3 DOWNTO 0);
		CLOCK_27	: in STD_LOGIC;
		CLOCK_50	: in STD_LOGIC;
		CLOCK_24	: in STD_LOGIC;
		EXT_CLOCK	: in STD_LOGIC;

		-- Output ports
		LEDR	: out STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDG	: out STD_LOGIC_VECTOR(7 DOWNTO 0);
		
		HEX0	: out STD_LOGIC_VECTOR(0 TO 7);
		HEX1	: out STD_LOGIC_VECTOR(0 TO 7);
		HEX2	: out STD_LOGIC_VECTOR(0 TO 7);
		HEX3	: out STD_LOGIC_VECTOR(0 TO 7);
		
		VGA_R, VGA_G, VGA_B	: OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		VGA_HS, VGA_VS		: OUT STD_LOGIC;
		
		PS2_DAT, PS2_CLK	: inout STD_LOGIC
		
	);
end demo_setup_vhd;


architecture Structure of demo_setup_vhd is

component InputController is
	port
	(
		------------------------	Clock Input	 	------------------------
		CLK				: 	in	STD_LOGIC;	--	24 MHz
					
		------------------------	PS2		--------------------------------
		PS2_DAT 		:	inout	STD_LOGIC;	--	PS2 Data
		PS2_CLK			:	inout	STD_LOGIC;		--	PS2 Clock

		MouseX			:	out STD_LOGIC_VECTOR(6 downto 0);
		MouseY			:	out STD_LOGIC_VECTOR(6 downto 0);
		MouseLeftClick	:	out STD_LOGIC;
		MouseRightClick	:	out STD_LOGIC
	);
end component;


component MapController is
	generic(
		MapDimension		:		INTEGER := 10;
		TilePickBusSize		:		INTEGER := 4;
		CommandBusSize		:		INTEGER := 2;
		BombCountBusSize	:		INTEGER := 4
	);
	port
	(
		--Sinais de entrada para escrita nos Tiles:
		WriteCLK			: in	STD_LOGIC;
		Command				: in	STD_LOGIC_VECTOR(CommandBusSize - 1 downto 0);
		WriteToAll			: in	STD_LOGIC;
		TileWriteX			: in	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);
		TileWriteY			: in	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);

		--Sinais de entrada para leitura dos Tiles:
		ReadCLK				: in	STD_LOGIC;
		TileReadX			: in	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);
		TileReadY			: in	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);
		--Sinais de saida para leitura dos Tiles:
		TileBomb			: out	STD_LOGIC;
		TileStateBits		: out	STD_LOGIC_VECTOR(1 downto 0);
		CloseBombsCount		: out	STD_LOGIC_VECTOR(BombCountBusSize - 1 downto 0);




		--Sinais de saida para o GameController e MapCreator
		BombCount			: out	STD_LOGIC_VECTOR(BombCountBusSize - 1 downto 0);
		GameOver			: out	STD_LOGIC;
		Victory				: out	STD_LOGIC



	);
end component;-- Library Clause(s) (optional)

COMPONENT DisplayController IS
	generic(
		MapDimension		:		INTEGER := 10;
		TilePickBusSize		:		INTEGER := 4;
		BombCountBusSize	:		INTEGER := 4
	);
	PORT (
		MouseX				: in	STD_LOGIC_VECTOR(6 downto 0);
		MouseY				: in	STD_LOGIC_VECTOR(6 downto 0);
		--Requisicoes ao MapController:
		TileReadX			: out	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);
		TileReadY			: out	STD_LOGIC_VECTOR(TilePickBusSize - 1 downto 0);
		--Retorno do MapController:
		TileBomb			: in	STD_LOGIC;
		TileStateBits		: in	STD_LOGIC_VECTOR(1 downto 0);
		CloseBombsCount		: in	STD_LOGIC_VECTOR(BombCountBusSize - 1 downto 0);

		reset				: IN STD_LOGIC;

		clk27M				: IN STD_LOGIC;

		VGA_R, VGA_G, VGA_B	: OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		VGA_HS, VGA_VS		: OUT STD_LOGIC
	);
END COMPONENT;


signal tempTileWriteX,tempTileWriteY	: STD_LOGIC_VECTOR(3 downto 0);

signal tempTileReadX, tempTileReadY 	: STD_LOGIC_VECTOR(3 downto 0);
signal tempTileBomb						: STD_LOGIC;
signal tempTileStateBits				: STD_LOGIC_VECTOR(1 downto 0);
signal tempTileBombCount				: STD_LOGIC_VECTOR(3 downto 0);

signal tempMouseX						: STD_LOGIC_VECTOR(6 downto 0);
signal tempMouseY						: STD_LOGIC_VECTOR(6 downto 0);
signal tempMouseLeftClick				: STD_LOGIC;
signal tempMouseRightClick				: STD_LOGIC;

begin


	InputControllerInst: InputController
		port map
		(
			
			CLK				=>	CLOCK_24,
			PS2_DAT 		=>	PS2_DAT,
			PS2_CLK			=>	PS2_CLK,

			MouseX			=>	tempMouseX,
			MouseY			=>	tempMouseY,
			MouseLeftClick	=> tempMouseLeftClick,
			MouseRightClick	=> tempMouseRightClick
	
		);
		
		

	tempTileWriteX <= std_logic_vector(to_unsigned((to_integer(unsigned(tempMouseX)) -24)/8,4));
	tempTileWriteY <= std_logic_vector(to_unsigned(((128 -(to_integer(unsigned(tempMouseY)))) - 16)/8,4));



	MapControllerInst: MapController
		port map 
		(
			WriteCLK			=> tempMouseLeftClick OR tempMouseRightClick,
			WriteToAll			=> NOT KEY(3),
			Command				=> SW(1 DOWNTO 0),
			TileWriteX			=> tempTileWriteX,
			TileWriteY			=> tempTileWriteY,

			--Sinais de entrada para leitura dos Tiles:
			ReadCLK				=> CLOCK_27,
			TileReadX			=> tempTileReadX,
			TileReadY			=> tempTileReadY,
			--Sinais de saida para leitura dos Tiles:
			TileBomb			=> tempTileBomb,
			TileStateBits		=> tempTileStateBits,
			CloseBombsCount		=> tempTileBombCount,

			--Sinais de saida para o GameController e MapCreator
			BombCount			=> open,
			GameOver			=> open,
			Victory				=> open
		);
	
	
	DisplayControllerInst: DisplayController
		generic map(
			MapDimension		=> 10,
			TilePickBusSize		=> 4
		)
		PORT map(
			MouseX				=> tempMouseX,
			MouseY				=> tempMouseY,
			--Requisicoes ao MapController:
			TileReadX			=> tempTileReadX,
			TileReadY			=> tempTileReadY,
			--Retorno do MapController:
			TileBomb			=> tempTileBomb,
			TileStateBits		=> tempTileStateBits,
			CloseBombsCount		=> tempTileBombCount,

			reset				=> '1',

			clk27M				=> CLOCK_27,

			VGA_R				=> VGA_R,
			VGA_G				=> VGA_G,
			VGA_B				=> VGA_B,
			VGA_HS				=> VGA_HS,
			VGA_VS				=> VGA_VS
		);

end Structure;
