LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

entity InputController is
	port
	(
		------------------------	Clock Input	 	------------------------
		CLK				: 	in	STD_LOGIC;	--	24 MHz
					
		------------------------	PS2		--------------------------------
		PS2_DAT 		:	inout	STD_LOGIC;	--	PS2 Data
		PS2_CLK			:	inout	STD_LOGIC;		--	PS2 Clock

		MouseX			:	out STD_LOGIC_VECTOR(6 downto 0);
		MouseY			:	out STD_LOGIC_VECTOR(6 downto 0);
		MouseLeftClick	:	out STD_LOGIC;
		MouseRightClick	:	out STD_LOGIC
	);
end;

architecture struct of InputController is

	component mouse_ctrl
		generic(
			clkfreq		:	integer
		);
		port(
			ps2_data	:	inout	std_logic;
			ps2_clk		:	inout	std_logic;
			clk			:	in 		std_logic;
			en			:	in	 	std_logic;
			resetn		:	in 		std_logic;
			newdata		:	out		std_logic;
			bt_on		:	out		std_logic_vector(2 downto 0);
			ox, oy		:	out 	std_logic;
			dx, dy		:	out		std_logic_vector(8 downto 0);
			wheel		:	out		std_logic_vector(3 downto 0)
		);
	end component;
	
	signal CLOCK_100, CLOCKHZ, signewdata, resetn : std_logic;
	signal dx, dy : std_logic_vector(8 downto 0);
	signal x, y 	: std_logic_vector(6 downto 0);
	
	constant SENSIBILITY : integer := 16; -- Rise to decrease sensibility
begin 
	-- KEY(0) Reset
	resetn <= '1';
	
	mousectrl : mouse_ctrl
		generic map (24000)
		port map(
			ps2_data	=>PS2_DAT,
			ps2_clk		=>PS2_CLK,
			clk			=>CLK,
			en			=>'1',
			resetn		=>resetn,
			newdata		=>signewdata,
			bt_on(2)	=> open,
			bt_on(1)	=> MouseRightClick,
			bt_on(0)	=> MouseLeftClick,
			ox			=>open,
			oy			=>open,
			dx			=>dx,
			dy			=>dy,
			wheel		=>open
	);
	
	-- Read new mouse data	
	process(signewdata, resetn)
		variable xacc, yacc : integer range -10000 to 10000;
	begin
		if(rising_edge(signewdata)) then			
			x <= std_logic_vector(to_signed(to_integer(signed(x)) + ((xacc + to_integer(signed(dx))) / SENSIBILITY), 7));
			y <= std_logic_vector(to_signed(to_integer(signed(y)) + ((yacc + to_integer(signed(dy))) / SENSIBILITY), 7));
			xacc := ((xacc + to_integer(signed(dx))) rem SENSIBILITY);
			yacc := ((yacc + to_integer(signed(dy))) rem SENSIBILITY);					
		end if;
		if resetn = '0' then
			xacc := 0;
			yacc := 0;
			x <= (others => '0');
			y <= (others => '0');
		end if;
	end process;

	MouseX <= x;
	MouseY <= y;

	-- 100 KHz clock	
	process(CLK)
		variable count : integer range 0 to 240 := 0;		
	begin
		if(CLK'event and CLK = '1') then
			if count < 240 / 2 then
				CLOCK_100 <= '1';
			else 
				CLOCK_100 <= '0';
			end if;
			if count = 240 then
				count := 0;
			end if;
			count := count + 1;			
		end if;
	end process;
	
	
	-- Hz clock	
	process(CLK)
		constant F_HZ : integer := 1000000;
		
		constant DIVIDER : integer := 24000000/F_HZ;
		variable count : integer range 0 to DIVIDER := 0;		
	begin
		if(rising_edge(CLK)) then
			if count < DIVIDER / 2 then
				CLOCKHZ <= '1';
			else 
				CLOCKHZ <= '0';
			end if;
			if count = DIVIDER then
				count := 0;
			end if;
			count := count + 1;			
		end if;
	end process;	
end struct;