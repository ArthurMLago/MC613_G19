-- latch SR com NOR A = R e B = S - CHAVEADO --

library ieee;
use ieee.std_logic_1164.all;

entity latchD is
	
	port
	(
		-- Input ports
		D, clk	: in  STD_LOGIC;
		
		-- Output ports
		Q		: out STD_LOGIC
	);
end latchD;

-- architecture
architecture Behavior of latchD is
	
begin
	process (D, clk)
	begin

		if clk = '1' then
			Q <= D;
		end if;

	end process;

end Behavior;
 