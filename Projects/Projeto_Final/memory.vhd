-- memoria de 128B do lab 10 reaproveitado --

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

-- entity da memoria --
entity memory is

	GENERIC 
	(
		WORDSIZE		: NATURAL	:= 8;
		BITS_OF_ADDR	: NATURAL	:= 16;
		MIF_FILE		: STRING	:= ""
	);

	PORT 
	(
		clock   : IN	STD_LOGIC;
		we      : IN	STD_LOGIC;
		address : IN	STD_LOGIC_VECTOR(BITS_OF_ADDR-1 DOWNTO 0);
		datain  : IN	STD_LOGIC_VECTOR(WORDSIZE-1 DOWNTO 0);
		dataout : OUT	STD_LOGIC_VECTOR(WORDSIZE-1 DOWNTO 0)
	);
	
end memory;


architecture Behaviour of memory is 

	-- instanciação da memoria ram mesmo, com profundidade = 2^n --
	type mem_type is array (0 to 2**BITS_OF_ADDR - 1) of std_logic_vector(WORDSIZE -1 downto 0);

	-- agora declaramos um sinal de memoria a ser usado aqui --
	signal mem : mem_type;

	-- tomariamos um atributo de inicio e definiriamos o arquivo de inicio de memoria com ele --
	attribute ram_init_file : string; -- definição do atributo
	attribute ram_init_file of mem : signal is MIF_FILE; -- especificação do atributo, tendo mif_file

begin 
	process (clock, address)
	begin
		if clock'event and clock = '1' then
			if we = '1' then
				-- converte para um inteiro e esse inteiro eh convertido a unsigned!! --
				mem(to_integer(unsigned(address))) <=  datain; 		
			end if ; 	
		end if ; 

		dataout <= mem(to_integer(unsigned(address)));
	end process;

end Behaviour;
