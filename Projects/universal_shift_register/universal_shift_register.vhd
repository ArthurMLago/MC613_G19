-- universal shift register --
library ieee;
use ieee.std_logic_1164.all;

entity universal_shift_register is
	
	generic (N : integer := 6);
	port
	(
		-- Input ports
		modo : 			in std_logic_vector(1 downto 0); -- op mode
		ParIn:			in std_logic_vector(N - 1 downto 0); -- a entrada em si
		SerIn: 			in std_logic; -- o bit que entra nos shifts 
		clk:			in std_logic; -- clock
		-- Output ports
		ParOut:			buffer std_logic_vector(N - 1 downto 0)
	);
end universal_shift_register;

architecture Behavior of universal_shift_register is

begin

	process(modo, ParIn, SerIn, clk)
	begin

		if clk'event and clk = '1' then
			-- NOP (mantem o valor atual) --
			if modo = "00" then
				ParOut <= ParOut;

			-- shift left --
			elsif modo = "01" then

				-- for loop mesmo --
				Left: for i in 0 to N-2 loop
					ParOut(i) <= ParOut(i + 1); 
				end loop;
				-- ultimo bit com a entrada --
				ParOut(N - 1) <= SerIn;
			
			-- shift right --
			elsif modo = "10" then

				-- for loop mesmo --
				Right: for i in 0 to N-2 loop
					ParOut(i + 1) <= ParOut(i); 
				end loop;
				-- ultimo bit com a entrada --
				ParOut(0) <= SerIn;
				
			-- carga paralela --
			elsif modo = "11" then
				ParOut <= ParIn;
			end if ;
		end if;

	end process;

end Behavior;

