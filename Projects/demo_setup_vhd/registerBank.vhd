LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY G19_packages;
USE G19_packages.decoders_package.all;
USE G19_packages.registers_package.all;
USE G19_packages.tristate_package.all;

entity registerBank is
	port
	(
		-- Input ports
		Wr_En, Rd_En	: in  STD_LOGIC;
		DATA_IN			: in  STD_LOGIC_VECTOR(3 downto 0);
		Clock			: in  STD_LOGIC;

		WriteSelect		: in  STD_LOGIC_VECTOR(2 downto 0);
		ReadSelect		: in  STD_LOGIC_VECTOR(2 downto 0);


		-- Output ports
		DATA_OUT		: out STD_LOGIC_VECTOR(3 downto 0)
	);
end registerBank;

architecture Structure of registerBank is

	signal D2R	:STD_LOGIC_VECTOR(7 downto 0);
	signal R2D	:STD_LOGIC_VECTOR(7 downto 0);

begin
	
	GenerateRegisters: 
		for i in 7 downto 0 generate
			signal R2T	:STD_LOGIC_VECTOR(3 downto 0);

			begin
				Registers: nRegLsRa
					generic map
					(
						N => 4
					)
					port map 
					(
						 D		=> DATA_IN,
						 Clock	=> Clock,
						 Reset	=> '0',
						 Load	=> D2R(i),
						 Q		=> R2T
					);

				Tristates: tristate
					generic map
					(
						N => 4
					)
					port map
					(
						e => R2D(i),
						x => R2T,
						f => DATA_OUT
					);
		end generate;

	InputRegisterSelector : decoder3bits
		port map 
		(
			 Enable	=> Wr_En,
			 S		=> WriteSelect,
			 X		=> D2R
		);

	OutputRegisterSelector : decoder3bits
		port map 
		(
			Enable	=> Rd_En,
			S		=> ReadSelect,
			X		=> R2D
		);

end Structure;
