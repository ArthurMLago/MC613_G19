library ieee;
use ieee.std_logic_1164.all;

entity demo_setup_vhd is
	port
	(
		-- Input ports
		SW			: in  STD_LOGIC_VECTOR(9 DOWNTO 0);
		KEY			: in  STD_LOGIC_VECTOR(3 DOWNTO 0);
		CLOCK_27	: in STD_LOGIC;
		CLOCK_50	: in STD_LOGIC;
		CLOCK_24	: in STD_LOGIC;
		EXT_CLOCK	: in STD_LOGIC;

		-- Output ports
		LEDR	: out STD_LOGIC_VECTOR(9 DOWNTO 0);
		LEDG	: out STD_LOGIC_VECTOR(7 DOWNTO 0);
		
		HEX0	: out STD_LOGIC_VECTOR(0 TO 7);
		HEX1	: out STD_LOGIC_VECTOR(0 TO 7);
		HEX2	: out STD_LOGIC_VECTOR(0 TO 7);
		HEX3	: out STD_LOGIC_VECTOR(0 TO 7)
		
	);
end demo_setup_vhd;


architecture Structure of demo_setup_vhd is

component registerBank is
	port
	(
		-- Input ports
		Wr_En, Rd_En	: in  STD_LOGIC;
		DATA_IN			: in  STD_LOGIC_VECTOR(3 downto 0);
		Clock			: in  STD_LOGIC;

		WriteSelect		: in  STD_LOGIC_VECTOR(2 downto 0);
		ReadSelect		: in  STD_LOGIC_VECTOR(2 downto 0);


		-- Output ports
		DATA_OUT		: out STD_LOGIC_VECTOR(3 downto 0)
	);
end component;

begin
	regBank : registerBank
		port map 
		(
			Wr_En		=> KEY(0),
			Rd_En		=> KEY(1),
			DATA_IN		=> SW(9 downto 6),
			Clock		=> CLOCK_27,
			WriteSelect	=> SW(2 downto 0),
			ReadSelect	=> SW(5 downto 3),
			DATA_OUT	=> LEDR(3 downto 0)
		);

end Structure;
