library ieee;
use ieee.std_logic_1164.all;

library G19_packages;
use G19_packages.adders_package.all;

entity ULA is

	port
	(
		A, B	: in  STD_LOGIC_VECTOR(3 downto 0);
		S		: in  STD_LOGIC_VECTOR(0 to 1);

		F		: out STD_LOGIC_VECTOR(3 downto 0);
		Z,C,V,N	: out STD_LOGIC
	);
end ULA;

architecture Structure of ULA is

	signal	SubFlag	: STD_LOGIC;
	--Sinal para guardar o complemento de 1 de B:
	signal 	Bcomp	: STD_LOGIC_VECTOR(3 downto 0);
	
	signal	Farit	: STD_LOGIC_VECTOR(3 downto 0);
	signal	Fland	: STD_LOGIC_VECTOR(3 downto 0);
	signal	Flor	: STD_LOGIC_VECTOR(3 downto 0);
	
	signal	Ftemp	: STD_LOGIC_VECTOR(3 downto 0);
	
	
	

begin
	SubFlag <= S(0) and not S(1);

	Bcomp <= B xor (B'Range => SubFlag);
	
	Adder: adder_4bits port map
		(SubFlag, A, Bcomp, Farit, C, V);
		
	Fland <= A and B;
	
	Flor <= A or B;
		
	
	Ftemp <= Farit when s(1) = '0' else 
		 Fland when (s(0) = '0' and s(1) = '1') else
		 Flor  when (s(0) = '1' and s(1) = '1') else
		 (others => 'Z');
	
	
	N <= Ftemp(3);
	Z <= '1' when Ftemp = "0000" else '0';
	
	F <= Ftemp;
	
	


end Structure;
