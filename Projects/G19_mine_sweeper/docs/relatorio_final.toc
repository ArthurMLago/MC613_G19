\select@language {brazil}
\contentsline {section}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{2}{section.1}
\contentsline {section}{\numberline {2}Teoria aplicada ao projeto}{2}{section.2}
\contentsline {section}{\numberline {3}Estrutura e implementa\IeC {\c c}\IeC {\~a}o do jogo}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Descri\IeC {\c c}\IeC {\~a}o dos m\IeC {\'o}dulos do jogo}{4}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Game Controller}{4}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Map Controller e Map Creator}{5}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Uso do LFSR (\textit {Linear-feedback Shift Register}) na cria\IeC {\c c}\IeC {\~a}o de mapas}{6}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Tile Controller}{7}{subsubsection.3.1.4}
\contentsline {subsubsection}{\numberline {3.1.5}Input Controller}{8}{subsubsection.3.1.5}
\contentsline {subsubsection}{\numberline {3.1.6}Display Controller}{8}{subsubsection.3.1.6}
\contentsline {subsection}{\numberline {3.2}Uso do componente vga}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Uso do mouse}{10}{subsection.3.3}
\contentsline {section}{\numberline {4}Conclus\IeC {\~o}es}{10}{section.4}
