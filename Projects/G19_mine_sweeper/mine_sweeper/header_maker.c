#include "stdio.h"

int main(int argc, char const *argv[])
{
	FILE *file;

	char description[150];

	printf("Executando %s para %s\n", argv[0], argv[1]);

	printf("-----------------------------------------------\n");
	printf("Descricao do header:\n");
	fgets(description, 150, NULL);

	file = fopen(argv[1], "r+");

	if (file)
	{
		fprintf(file, "-----------------------------------------------------------------\n
						-- UNIVERSIDADE ESTADUAL DE CAMPINAS - UNICAMP\n
						-- Trabalho Final de MC613 (Laboratorio de Circuitos Digitais)\n
						-- \n
						-- Autores: Arthur Moraes Do Lago & Beatriz Sechin Zazulla\n
						-- RAs: 157702 & 154779\n
						-- \n
						-- Description: %s\n
						-----------------------------------------------------------------"\n\n, description);

		fclose(file);
	}
	else
		printf("Erro ao abrir o arquivo.\n");

	return 0;
}