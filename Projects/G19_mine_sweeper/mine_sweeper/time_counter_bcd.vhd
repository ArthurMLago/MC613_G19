----------------------------------------------------------------
-- 
--
--
--
--
----------------------------------------------------------------

-- contador bcd de dois d�gitos --

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity time_counter_bcd is
	
	port
	(
		-- Input ports
		clk, enable, clear	: in  std_logic;

		-- Output ports - two 7-seg displays should be nice
		bcd1, bcd2	: buffer std_logic_vector(3 downto 0)
		
	);
	
end time_counter_bcd;


architecture Behavior of time_counter_bcd is

begin

	process(clk, enable, clear, bcd1, bcd2)
	begin
		-- em caso de borda de subida de clock --
		if clk'event and clk = '1' then
			if clear = '1' then
				bcd1 <= "0000";
				bcd2 <= "0000";
			else
				if bcd1 = "1001" then -- 9
					bcd1 <= "0000";
					if bcd2 = "1001" then 
						bcd2 <= "0000";
					else
						bcd2 <= bcd2 + 1;
					end if;
				else
					bcd1 <= bcd1 + 1;
				end if;
			end if;
		end if;
	end process;

end Behavior;
