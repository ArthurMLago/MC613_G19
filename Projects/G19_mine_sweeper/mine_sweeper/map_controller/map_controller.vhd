---------------------------------------------------------------------------------------------------------------------------------
-- UNIVERSIDADE ESTADUAL DE CAMPINAS - UNICAMP
-- TRABALHO FINAL DE MC613 (LAB DE CIRCUITOS DIGITAIS)
-- Primeiro Semestre de 2016
--
-- Autores: Arthur Moraes do Lago & Beatriz Sechin Zazulla
-- RA's: 157702 & 154779
--
-- MAP CONTROLLER
-- Descrição: Controle da mudança de estado do mapa. Esse móodulo detém a matriz do mapa e é capaz de operar em suas posições.
----------------------------------------------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-- entity--

entity map_controller is
	port 
	(
		-- input ports --

		-- sinal map_creating recebido do map creator para indicar se um mapa está sendo criado ou não --
		map_creating : in std_logic;

		-- MAP CREATOR --
		-- sinal de clock advindo de map creator (o uso deste ou do clk do game controller dependerá do sinal map_creating)
		mc_clk : in std_logic;
		-- seleção do tile advinda do map creator (o uso desta ou da seleção do game controller dependerá do sinal map_creating) --
		mc_tile_x, mc_tile_y : in std_logic_vector(6 downto 0);
		-- ação que será tomada com o tile selecionado advinda do map creator (o uso desta ou da ação do game controller dependerá do sinal map_creating) --
		mc_tile_action : in std_logic_vector(1 downto 0);

		-- GAME CONTROLLER --
		-- mesmos sinais para o caso de map_creating ser 0 --
		gc_clk : in std_logic;
		gc_tile_x, gc_tile_y : in std_logic_vector(6 downto 0);
		gc_tile_action : in std_logic_vector(1 downto 0);

		-- DISPLAY CONTROLLER --
		-- map controller recebe informações de cada tile --
		dc_tile_x, dc_tile_y : in std_logic_vector(6 downto 0);


		-- output ports --

		-- DISPLAY CONTROLLER --
		-- estado do tile requisitado --
		dc_tiled_display : out std_logic_vector(3 downto 0);
		-- sinal para indicar que o estado do tile esta pronto para ser lido --
		dc_ready_to_read : out std_logic;

		-- GAME CONTROLLER RECEBE --
		-- se houve ou não perda --
		lost : out std_logic;
		-- se houve vitória --
		win : out std_logic;


		-- GAME CONTROLLER - acho que não ta certo isso  --
		gc_tile_state : out STD_LOGIC_VECTOR;
		gc_ready_to_read : out std_logic;

		-- sinal de contagem de minas para o game_controller e map_creator --
		mine_count : out std_logic_vector(13 downto 0)
	);
end map_controller;