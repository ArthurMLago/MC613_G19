---------------------------------------------------------------------------------------------------------------------------------
-- UNIVERSIDADE ESTADUAL DE CAMPINAS - UNICAMP
-- TRABALHO FINAL DE MC613 (LAB DE CIRCUITOS DIGITAIS)
-- Primeiro Semestre de 2016
--
-- Autores: Arthur Moraes do Lago & Beatriz Sechin Zazulla
-- RA's: 157702 & 154779
--
-- MAP CREATOR
-- Descrição: Módulo responsável pela criação dos mapas dadas as dimensões e o número de minas contidas.
----------------------------------------------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity map_creator is
	port
	(
		-- input ports --

		-- GAME CONTROLLER MANDA --
		-- tamanho do mapa a ser criado (serao mapas quadrados) --
		map_size : in std_logic_vector(6 downto 0);
		-- numero de minas a serem distribuidas no mapa --
		mine_quantity : in std_logic_vector(13 downto 0);
		-- sinal de ativação da criação --
		create_map : in std_logic;

		-- output ports --

		-- MAP CONTROLLER --
		-- sinal de clock advindo de map creator (o uso deste ou do clk do game controller dependerá do sinal map_creating)
		mc_clk : out std_logic;
		-- seleção do tile advinda do map creator (o uso desta ou da seleção do game controller dependerá do sinal map_creating) --
		mc_tile_x, mc_tile_y : out std_logic_vector(6 downto 0);
		-- ação que será tomada com o tile selecionado advinda do map creator (o uso desta ou da ação do game controller dependerá do sinal map_creating) --
		mc_tile_action : out std_logic_vector(1 downto 0);

		-- GAME CONTROLLER --
		-- sinal que indica que o mapa está pronto para ser usado --
		got_map: out std_logic

	);
end map_creator;