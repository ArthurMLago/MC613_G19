---------------------------------------------------------------------------------------------------------------------------------
-- UNIVERSIDADE ESTADUAL DE CAMPINAS - UNICAMP
-- TRABALHO FINAL DE MC613 (LAB DE CIRCUITOS DIGITAIS)
-- Primeiro Semestre de 2016
--
-- Autores: Arthur Moraes do Lago & Beatriz Sechin Zazulla
-- RA's: 157702 & 154779
--
-- GAME CONTROLLER
-- Descricao: Componente responsahvel pela oraganizacao basica do jogo e pelo controle de quase todos os outros módulos. Trata-se
-- de um conponente de comunicacao universal, com as entradas, saídas e módulos de processamento.
----------------------------------------------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library G19_packages;
use G19_packages.flipflops_package.all;
use G19_packages.counters_package.all;

-- entity--
entity game_controller is
	port 
	(
		-- input ports --

		-- clock --
		clk : in std_logic;

		-- INPUT CONTROLLER MANDA --
		-- reset sincrono ativo alto --
		Reset : in std_logic;
		-- posicao do cursor do mouse (em qual tile se encontra) ou teclado no mapa - no maior dos mapas havera 128 tiles --
		tile_clicked_pos_x, tile_clicked_pos_y : in std_logic_vector(6 downto 0);
		-- action type - tipo de acao tomada pelo jogador: abrir o tile ou marcar o tile como mina --
		action_type : in std_logic_vector(1 downto 0);

		-- MAP CONTROLLER MANDA --
		-- se houve ou nao perda --
		lost : in std_logic;
		-- se houve vitória --
		win : in std_logic;

		-- MAP CREATOR MANDA --
		-- sinal que diz que um mapa jah foi construido e o jogo estah pronto para comecar --
		got_map: in std_logic;

		-- output ports --

		-- MAP CONTROLLER RECEBE --
		-- selecao do tile que deverah ser operado por map controlLer --
		tile_select_x, tile_select_y : out std_logic_vector(6 downto 0);
		-- acao que serah tomada com o tile selecionado passado ao map controller --
		tile_action : out std_logic_vector(1 downto 0);

		-- MAP CREATOR RECEBE --
		-- sinal de ativacao da criacao --
		create_map : out std_logic;
		-- o tamanho e a quantidade de minas � fixa

		-- DISPLAY CONTROLLER RECEBE --
		-- estado atual do jogo  - para desenhar algo especial na tela --
		-- 00 -> Waiting2Start
		-- 01 -> Game (normal)
		-- 10 -> Win
		-- 11 -> GameOver
		game_state : out std_logic_vector(1 downto 0);
		-- contagem de tempo em segundos para aparecer na tela --
		time_count : buffer integer range 0 to 999
	);
end game_controller;

architecture Behavior of game_controller is

	-- ESTADOS DE JOGO -- 
	-- Waiting2Start = espera para comecar
	-- Game = em jogo
	-- GameOver = fim de jogo
	-- WinGame = o jogo foi vencido 
	type State_type is (Waiting2Start, Game, WinGame, GameOver);

	-- sinal para o estado atual do jogo dentro da programacao --
	signal current_state : State_type;

	-- para o contador de tempo embaixo (recebe a saída do contador)
	signal counter_out : std_logic_vector(25 downto 0);
	
begin

	-- MAQUINA DE ESTADOS DE GAME CONTROLLER --
	process (Reset, clk, counter_out)
	begin
		-- todas as alteracoes sao sincronas --
		if (clk'event and clk = '1') then

			-- RESET --
			if Reset = '1' then
				current_state <= Waiting2Start;
				time_count <= 0;

			-- MUDANCAS DE ESTADO DO JOGO --
			else
				-- para game --
				case got_map is
					when '1' => current_state <= Game;
					when '0' => current_state <= current_state;
				end case;

				-- para GameOver --
				case lost is
					when '1' => current_state <= GameOver;
					when '0' => current_state <= current_state;
				end case;

				-- para WinGame --
				case win is
					when '1' => current_state <= WinGame;
					when '0' => current_state <= current_state;
				end case;
			end if;
		end if;
		
		
		-- mudan�a do tempo em segundos - monitorando counter_out --
		case counter_out is
				when "10111110101111000010000000" => time_count <= time_count + 1;
				when others => time_count <= time_count;
		end case;
		
	end process;
	

	-- ACOES SOBRE ESTADOS EM GAME CONTROLLER --
	process (current_state)
	begin
		case current_state is

			-- Waiting2Start --
			when Waiting2Start => 

				create_map <= '1'; -- pede criacao de mapa (habilita o componente map_creator)
				game_state <= "00"; -- display controller pode ignorar isso e deixar como uma tela inicial ou tela de jogo ja

			-- Game --
			when Game =>

				-- manda posicões do cursor do teclado ou mouse para map controller, alem de mandar o tipo de acao tomada (que veio do input controller) --
				-- map controller sempre estah enabled --

				tile_select_x <= tile_clicked_pos_x;
				tile_select_y <= tile_clicked_pos_y;
				tile_action <= action_type;

				game_state <= "01";

			-- GameOver --
			when GameOver =>

				-- Display deverah colocar a tela de perdeu, e esperar pelo jogador comecar de novo dando reset 
				game_state <= "11";

			-- WinGame --
			when WinGame =>
				
				-- Display deverah colocar a tela de ganhou, e esperar pelo jogador comecar de novo dando reset 
				game_state <= "10";
		end case;
	end process;

	-- contador de tempo --

	time_counter: genericCounter 
		generic map 
		(
			N => 26 -- numero de bits do contador modulo 50.000.000 (clock de 50MHz)
		)
		port map
		(
			-- Input ports
			Enable						=> '1',	
			Clock 						=> clk,	
			Reset 						=> counter_out(7) and counter_out(12) and 
										counter_out(13) and counter_out(14) and
										counter_out(15) and counter_out(17) and 
										counter_out(19) and counter_out(20) and 
										counter_out(21) and counter_out(22) and 
										counter_out(23) and counter_out(25),	-- mod 50.000.000 -> ‭0010111110101111000010000000‬ em binario
			-- Output ports
			Qout						=> counter_out,	-- contador do tempo atual

			Cout						=> open	-- Cout não vai para nada importante
		);

end Behavior;