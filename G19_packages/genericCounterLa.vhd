LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY G19_packages;
USE G19_packages.flipflops_package.all;

entity genericCounterLa is
	generic
	(
		N		: INTEGER  :=	4
	);
	port
	(
		-- Input ports
		Enable	: in STD_LOGIC;
		Clock	: in STD_LOGIC;
		Reset	: in STD_LOGIC;
		Load	: in STD_LOGIC;
		Data	: in STD_LOGIC_VECTOR(N - 1 downto 0);

		-- Output ports
		Qout	: out STD_LOGIC_VECTOR(N - 1 downto 0);

		Cout	: out STD_LOGIC
	);
end genericCounterLa;

architecture Structure of genericCounterLa is

	signal Q	: STD_LOGIC_VECTOR(N - 1 downto 0);
	signal C	: STD_LOGIC_VECTOR(N downto 0);

begin

	C(0) <= Enable;

	GenerateFFs: 
		for i in N - 1 downto 0 generate
			begin
			C(i + 1) <= C(i) AND Q(i);

			FlipFlops: flipflopD
				port map(
					D => Q(i) XOR C(i),
					preset => Load AND Data(i),
					reset => (Load AND NOT Data(i)) OR Reset,
					clk => Clock,
					Q => Q(i),
					Qn => open
				);
		end generate;

		Qout <= Q;




end Structure;
