-- Carry Look Ahead adder de 4 bits --

library ieee;
use ieee.std_logic_1164.all ;

entity CLAadder_4bits is
	port
	(
		-- Input ports
		X, Y	: in  STD_LOGIC_VECTOR(3 DOWNTO 0);
		Cin		: in  STD_LOGIC;

		-- Output ports
		S		: out STD_LOGIC_VECTOR(3 DOWNTO 0);
		Cout	: out STD_LOGIC
	);
end CLAadder_4bits;

-- Library Clause(s) (optional)
-- Use Clause(s) (optional)

architecture Structure of CLAadder_4bits is

	signal g,p	: STD_LOGIC_VECTOR(3 DOWNTO 0);
	signal Cmid	: STD_LOGIC_VECTOR(3 DOWNTO 1);

begin
	
	g <= X AND Y;
	p <= X OR Y;


	Cmid(1) <= g(0) OR (p(0) AND Cin);
	Cmid(2) <= g(1) OR (p(1) AND g(0)) OR (p(1) AND p(0) AND Cin);
	Cmid(3) <= g(2) OR (p(2) AND g(1)) OR (p(2) AND p(1) AND g(0)) OR (p(2) AND p(1) AND p(0) AND Cin);
	Cout 	<= g(3) OR (p(3) AND g(2)) OR (p(3) AND p(2) AND g(1)) OR (p(3) AND p(2) AND p(1) AND g(0)) OR (p(3) AND p(2) AND p(1) AND p(0) AND Cin);

	S(0) <= X(0) XOR Y(0) XOR Cin;
	S(1) <= X(1) XOR Y(1) XOR Cmid(1);
	S(2) <= X(2) XOR Y(2) XOR Cmid(2);
	S(3) <= X(3) XOR Y(3) XOR Cmid(3);
	

end Structure;
