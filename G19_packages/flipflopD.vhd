-- flip flop D com preset e set assincronos ativo altos --

library ieee;
use ieee.std_logic_1164.all;

entity flipflopD is
	
	port
	(
		-- Input ports
		D, preset, reset: 	in std_logic;
		clk: 				in std_logic;
		-- Output ports
		Q, Qn	: out std_logic
	);
end flipflopD;

architecture Behavior of flipflopD is

begin

	process(preset, reset, clk)
	begin
		
		-- monitoramento do clock (quando nao ha preset ou reset) --
		if reset = '0' and preset = '0' then
			if (clk'event and clk = '1') then
				Q <= D;
				Qn <= not D;
			end if;

		-- reset assincrono (nao depende do clock) --
		elsif reset = '1' and preset = '0' then
			Q <= '0';
			Qn <= '1';
		
		-- preset assincrono (nao depende do clock) --
		elsif reset = '0' and preset = '1' then
			Q <= '1';
			Qn <= '0';

		end if;
	end process;
end Behavior;

