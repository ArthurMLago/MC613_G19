LIBRARY ieee ;
USE ieee.std_logic_1164.all ;

-- DISPLAY DE 7 SEGMENTOS --
PACKAGE disp7seg_package IS
	COMPONENT disp7seg_dec
		PORT(
			SW : IN STD_LOGIC_VECTOR(3 DOWNTO 0) ;
			HEX0 : OUT STD_LOGIC_VECTOR(0 TO 7)
		);
	END COMPONENT;
	COMPONENT disp7seg_hex
		PORT(
			SW : IN STD_LOGIC_VECTOR(3 DOWNTO 0) ;
			HEX0 : OUT STD_LOGIC_VECTOR(0 TO 7)
		);
	END COMPONENT;

END disp7seg_package;

LIBRARY ieee ;
USE ieee.std_logic_1164.all ;

-- MULTIPLEXADORES --
PACKAGE mux_package IS
	COMPONENT mux4to1
		PORT 
		( 
			w0, w1, w2, w3: 	IN STD_LOGIC ;
			s: 					IN STD_LOGIC_VECTOR(1 DOWNTO 0) ;
			f:					OUT STD_LOGIC 
		) ;
	END COMPONENT ;
END mux_package ;


-- SOMADORES --
LIBRARY ieee ;
USE ieee.std_logic_1164.all ;

PACKAGE adders_package IS
	COMPONENT full_adder_estrutural
		PORT 
		(
			Cin, x, y :			 IN STD_LOGIC ;
			s, Cout :			 OUT STD_LOGIC
		) ;
	END COMPONENT;

	COMPONENT generic_adder IS
		GENERIC (
			N				: INTEGER  := 32
		);
		PORT (
			Cin				: IN STD_LOGIC;
			X, Y			: IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
			S				: OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
			Cout, Overflow	: OUT STD_LOGIC
			);
	END COMPONENT;


	COMPONENT CLAadder_4bits
		PORT
		(	
			X, Y	: 			in  STD_LOGIC_VECTOR(3 DOWNTO 0);
			Cin		: 			in  STD_LOGIC;
			S		: 			out STD_LOGIC_VECTOR(3 DOWNTO 0);
			Cout	: 			out STD_LOGIC	
		);
	END COMPONENT;
END adders_package;

LIBRARY ieee ;
USE ieee.std_logic_1164.all ;

-- DESLOCADORES --

PACKAGE barrel_shifter_4bits_package IS
	COMPONENT barrel_shifter_4bits
		port 
		(
			s : 				in std_logic_vector(1 downto 0);
			w : 				in std_logic_vector(3 downto 0);
			y : 				out std_logic_vector(3 downto 0)
		);
	END COMPONENT ;
END  barrel_shifter_4bits_package;

-- REGISTRADORES --

LIBRARY ieee ;
USE ieee.std_logic_1164.all ;

PACKAGE registers_package IS
	--------------------------------------------------
	--Registrador com Load síncrono e Reset Assincrono
	COMPONENT nRegLsRa
		GENERIC(
			N : INTEGER := 8 
		);
		PORT(
			D			: IN  STD_LOGIC_VECTOR(N - 1 DOWNTO 0) ;
			Clock		: IN  STD_LOGIC ;
			Reset,Load	: IN  STD_LOGIC ;
			Q			: OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0) 
		);
	END COMPONENT;
END registers_package;

LIBRARY ieee ;
USE ieee.std_logic_1164.all ;

PACKAGE decoders_package is
	component generic_decoder IS
		GENERIC (
			InputBusSize	: INTEGER  := 5
		);
		PORT (
			S				: IN STD_LOGIC_VECTOR(InputBusSize -1 DOWNTO 0);
			Enable			: IN STD_LOGIC;

			D				: buffer STD_LOGIC_VECTOR(2**InputBusSize - 1 DOWNTO 0)
		);
	END COMPONENT;
END decoders_package;

LIBRARY ieee ;
USE ieee.std_logic_1164.all ;

-- TRISTATE --

PACKAGE tristate_package is
	COMPONENT tristate
		generic (N	: integer  :=	4);
		port
		(
			-- Input ports
			e	: in STD_LOGIC;
			x	: in STD_LOGIC_VECTOR(N - 1 downto 0);
			
			-- Output ports
			f	: out STD_LOGIC_VECTOR(N - 1 downto 0)
		);
	END COMPONENT ;
END tristate_package;

-- FLIP FLOPS --

library ieee;
use ieee.std_logic_1164.all;

PACKAGE flipflops_package IS
	COMPONENT flipflopD
		port
		(
			D, preset, reset:	in  std_logic;
			clk: 				in  std_logic;
			Q, Qn:				out std_logic
		);
	END COMPONENT;

	COMPONENT flipflopJK
		port
		(
			J, K, clk	: 		in STD_LOGIC;
			preset, clear 	: 	in STD_LOGIC;
	
			Q, Qn		: 		buffer STD_LOGIC
		);
	END COMPONENT;

	COMPONENT flipflopT
		port
		(
			T, clk	: 			in STD_LOGIC;
			preset, clear 	: 	in STD_LOGIC;
	
			Q		: 			buffer STD_LOGIC
		);
	END COMPONENT;

END flipflops_package;

library ieee;
use ieee.std_logic_1164.all;

PACKAGE counters_package IS
	COMPONENT genericCounter is
		generic
		(
			N		: INTEGER  :=	4
		);
		port
		(
			Enable	: in STD_LOGIC;
			Clock	: in STD_LOGIC;
			Reset	: in STD_LOGIC;

			Qout	: out STD_LOGIC_VECTOR(N - 1 downto 0);

			Cout	: out STD_LOGIC
		);
	END COMPONENT;
	COMPONENT genericCounterLa is
		generic
		(
			N		: INTEGER  :=	4
		);
		port
		(
			-- Input ports
			Enable	: in STD_LOGIC;
			Clock	: in STD_LOGIC;
			Reset	: in STD_LOGIC;
			Load	: in STD_LOGIC;
			Data	: in STD_LOGIC_VECTOR(N - 1 downto 0);

			-- Output ports
			Qout	: out STD_LOGIC_VECTOR(N - 1 downto 0);

			Cout	: out STD_LOGIC
		);
	END COMPONENT;

END counters_package;

-- uso do monitor vga com a placa de1 --
library ieee;
use ieee.std_logic_1164.all;

PACKAGE vga_package IS
	COMPONENT vgacon IS
		generic 
		(
		    NUM_HORZ_PIXELS : natural := 128;  -- Number of horizontal pixels
		    NUM_VERT_PIXELS : natural := 96  -- Number of vertical pixels
		);	  
		port 
		(
		    clk27M, rstn              : in  std_logic;
		    write_clk, write_enable   : in  std_logic;
		    write_addr                : in  integer range 0 to NUM_HORZ_PIXELS * NUM_VERT_PIXELS - 1;
		    data_in                   : in  std_logic_vector(2 downto 0);
		    vga_clk                   : buffer std_logic;       -- Ideally 25.175 MHz
		    red, green, blue          : out std_logic_vector(3 downto 0);
		    hsync, vsync              : out std_logic
		);
	end component;
end vga_package;