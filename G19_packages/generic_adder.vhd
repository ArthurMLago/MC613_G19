-- somador de 8bits com indica��o de overflow --

LIBRARY ieee ;
USE ieee.std_logic_1164.all ;

LIBRARY G19_packages;
USE G19_packages.adders_package.full_adder_estrutural;

ENTITY generic_adder IS
	GENERIC (
		N				: INTEGER  := 32
	);
	PORT (
		Cin				: IN STD_LOGIC;
		X, Y			: IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
		S				: OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
		Cout, Overflow	: OUT STD_LOGIC
		);
END generic_adder;

ARCHITECTURE Structure OF generic_adder IS
	
	SIGNAL C : STD_LOGIC_VECTOR(1 TO N + 1);
	
	BEGIN
	
	C(1) <= Cin;
	
	G1: FOR i IN 1 to N GENERATE
		adders: full_adder_estrutural PORT MAP (C(i), X(i - 1), Y(i - 1), S(i - 1), C(i + 1)) ;
	END GENERATE;
		
	Overflow <= C(N) XOR C(N + 1);
	Cout <= C(N + 1);
	
END Structure ;