-- barrel shifter de 4bits utilizando mux 4:1 --

library ieee;
use ieee.std_logic_1164.all;

library G19_packages;
use G19_packages.mux4to1_package.all;

entity barrel_shifter_4bits is

	port (
	 	s : in std_logic_vector(1 downto 0);
		w : in std_logic_vector(3 downto 0);
		y : out std_logic_vector(3 downto 0)
	 );

end barrel_shifter_4bits;

architecture Behavior of barrel_shifter_4bits is

	begin
	mux1: mux4to1 
		port map (w(3), w(0), w(1), w(2), s(1 downto 0), y(3));
	
	mux2: mux4to1 
		port map (w(2), w(3), w(0), w(1), s(1 downto 0), y(2));
	
	mux3: mux4to1 
		port map (w(1), w(2), w(3), w(0), s(1 downto 0), y(1));
	
	mux4: mux4to1 
		port map (w(0), w(1), w(2), w(3), s(1 downto 0), y(0));
	
end Behavior ;