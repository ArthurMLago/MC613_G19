-- half adder utilizando vhdl estrutural - direto nas portas l�gicas --

LIBRARY ieee ;
USE ieee.std_logic_1164.all ;

ENTITY full_adder_estrutural IS
	PORT ( Cin, x, y : IN STD_LOGIC ;
	s, Cout : OUT STD_LOGIC ) ;
END full_adder_estrutural ;

ARCHITECTURE LogicFunc OF full_adder_estrutural IS
BEGIN
	s <= x XOR y XOR Cin ;
	Cout <= (x AND y) OR (Cin AND x) OR (Cin AND y) ;
END LogicFunc ;