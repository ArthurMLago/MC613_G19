 -- FF Toggle com T = B sensivel a borda de subida --
 -- Possui e preset e clear assincrono --
 -- preset e clear sao ativo baixos --

library ieee;
use ieee.std_logic_1164.all;

entity flipflopT is
	
	port
	(
		-- Input ports
		T, clk	: in  STD_LOGIC;
		preset, clear 	: in STD_LOGIC;
		
		-- Output ports
		Q		: buffer STD_LOGIC
	);
	
end flipflopT;

-- architecture
architecture Behavior of flipflopT is
	
begin
	process (Q, clk, preset, clear)
	begin

		-- preset assincrono --
		if preset = '0' then 
			Q <= '1';
		-- clear assincrono --
		elsif clear = '0' then
			Q <= '0';
		-- borda de subida --
		elsif clk'event and clk = '1' and T = '1' then
			Q <= not Q;
		end if;

	end process;

end Behavior;
 