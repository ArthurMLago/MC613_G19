--------------------------------------------------------------------------------
---------# Registrador de n-bits com Load s�ncrono e Reset assincrono #---------
--------------------------------------------------------------------------------

LIBRARY ieee ;
USE ieee.std_logic_1164.all ;


ENTITY nRegLsRa IS
	GENERIC ( N : INTEGER := 8 ) ;
	PORT(
		D			: IN  STD_LOGIC_VECTOR(N - 1 DOWNTO 0) ;
		Clock		: IN  STD_LOGIC ;
		Reset,Load	: IN  STD_LOGIC ;
		Q			: OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0) 
	);
END nRegLsRa;

ARCHITECTURE Behavior OF nRegLsRa IS
	BEGIN
	PROCESS (Clock, Reset)
		BEGIN

		IF Reset = '1' THEN
			Q <= (others => '0');
		ELSIF Clock'EVENT AND Clock = '1' AND Load = '1' THEN
			Q <= D ;
		END IF ;
	END PROCESS ;
END Behavior ;