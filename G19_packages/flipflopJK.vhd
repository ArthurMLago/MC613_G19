-- flip flop JK sensivel a borda de subida, com preset e clear assincronos --
-- preset e clear sao ativo baixos --

library ieee;
use ieee.std_logic_1164.all;

entity flipflopJK is
	
	port
	(
		-- Input ports
		J, K, clk	: in  STD_LOGIC;
		preset, clear 	: in STD_LOGIC;
		
		-- Output ports
		Q, Qn		: buffer STD_LOGIC
	);
end flipflopJK;

-- architecture
architecture Behavior of flipflopJK is
begin
	
	process (clk, preset, clear)
	
	variable jk: STD_LOGIC_VECTOR(1 downto 0);
	
	begin

		jk := J & K;
		-- preset assincrono --
		if preset = '0' then 
			Q <= '1';
			Qn <= '0';
		-- clear assincrono --
		elsif clear = '0' then
			Q <= '0';
			Qn <= '1';
		-- borda de subida --
		elsif clk'event and clk = '1' then
	
			-- comeca uma parte comportamental --
			case jk is 
				when "11" => Q <= not Q;
				when "10" => Q <= '1';
				when "01" => Q <= '0';
				when others => Q <= Q;
			end case;
			-- not Q --
			Qn <= not Q;
		end if;
		
	end process;

end Behavior;
